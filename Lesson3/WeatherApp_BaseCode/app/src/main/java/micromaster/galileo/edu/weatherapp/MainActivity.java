package micromaster.galileo.edu.weatherapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.IOException;

import micromaster.galileo.edu.weatherapp.API.WeatherInterface;
import micromaster.galileo.edu.weatherapp.model.WeatherData;
import micromaster.galileo.edu.weatherapp.model.WeatherResponse;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private final static String BASE_URL = "http://api.wunderground.com/api/";
    private final static String API_KEY = "203dd3bf35d8cf19";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new WheatherTask().execute();

    }
    private class WheatherTask extends AsyncTask<Void, Void, WeatherResponse> {
        @Override
        protected WeatherResponse doInBackground(Void... params) {
            //*************** TODO: MOVE THIS BLOCK OF CODE TO AN ASYNC_TASK**********
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            WeatherInterface weatherInterface = retrofit.create(WeatherInterface.class);
            Call<WeatherResponse> call = weatherInterface.getWeatherFromSanFrancisco(API_KEY);
            WeatherResponse weatherResponse = null;
            try {
                weatherResponse = call.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //*************************************************************************
            return weatherResponse;
        }

        protected void onPostExecute(WeatherResponse result) {
            WeatherData data = result.getWeatherData();

            TextView txtCountryName = (TextView) findViewById(R.id.countryName);
            txtCountryName.setText(data.getDisplayLocation().getCityName());

            TextView txtWeather = (TextView) findViewById(R.id.weather);
            txtWeather.setText(data.getWeather());

            TextView txtTemperature = (TextView) findViewById(R.id.temperature);
            txtTemperature.setText(data.getTemp());

            TextView txtPressure = (TextView) findViewById(R.id.pressure);
            txtPressure.setText(data.getPressure().toString());

            TextView txtHumidity = (TextView) findViewById(R.id.humidity);
            txtHumidity.setText(data.getHumidity());
        }
    }
}

