package micromaster.beginner.com.tipcalculator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Byron on 2/14/2017.
 */
public class TipCalculatorTest {

    private TipCalculator tipCalculator;

    @Before
    public void setUp() throws Exception {
        tipCalculator = new TipCalculator();
    }

    @Test
    public void calculateTip_15() throws Exception {
        Double billAmount = 100.0;
        Double tipToCalculate = 0.15;
        String totalExpected = "115.0";

        String totalAmount = tipCalculator.calculateTip(billAmount, tipToCalculate);

        //expected: 100 + (100*15%) = 115.0
        assertEquals(totalExpected, totalAmount);
    }

    @Test
    public void calculateTip_20() throws Exception {
        Double billAmount = 500.0;
        Double tipToCalculate = 0.20;
        String totalExpected = "600.0";

        String totalAmount = tipCalculator.calculateTip(billAmount, tipToCalculate);
        assertEquals(totalExpected, totalAmount);
    }

    @Test
    public void calculateTip_30() throws Exception {
        Double billAmount = 200.0;
        Double tipToCalculate = 0.30;
        String totalExpected = "260.0";

        String totalAmount = tipCalculator.calculateTip(billAmount, tipToCalculate);
        assertEquals(totalExpected, totalAmount);
    }

    @Test
    public void calculateTip_40() throws Exception {
        Double billAmount = 400.0;
        Double tipToCalculate = 0.40;
        String totalExpected = "560.0";

        String totalAmount = tipCalculator.calculateTip(billAmount, tipToCalculate);
        assertEquals(totalExpected, totalAmount);
    }


    @Test
    public void testParseBillValue() throws Exception {
        Double numberParsetest = TipCalculator.parseBillValue("88");
        assertEquals("88.0", numberParsetest.toString());
    }
}